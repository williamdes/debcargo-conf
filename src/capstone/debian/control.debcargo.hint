Source: rust-capstone
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-capstone-sys-0.15+default-dev <!nocheck>,
 librust-libc-0.2-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Michael R. Crusoe <crusoe@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/capstone]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/capstone
Rules-Requires-Root: no

Package: librust-capstone-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-capstone-sys-0.15+default-dev,
 librust-libc-0.2-dev
Suggests:
 librust-capstone+use-bindgen-dev (= ${binary:Version})
Provides:
 librust-capstone+default-dev (= ${binary:Version}),
 librust-capstone-0-dev (= ${binary:Version}),
 librust-capstone-0+default-dev (= ${binary:Version}),
 librust-capstone-0.11-dev (= ${binary:Version}),
 librust-capstone-0.11+default-dev (= ${binary:Version}),
 librust-capstone-0.11.0-dev (= ${binary:Version}),
 librust-capstone-0.11.0+default-dev (= ${binary:Version})
Description: Bindings to capstone disassembly engine - rust source code
 Bindings to the capstone library (https://www.capstone-engine.org/)
  disassembly framework.

Package: librust-capstone+use-bindgen-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-capstone-dev (= ${binary:Version}),
 librust-capstone-sys-0.15+use-bindgen-dev
Provides:
 librust-capstone-0+use-bindgen-dev (= ${binary:Version}),
 librust-capstone-0.11+use-bindgen-dev (= ${binary:Version}),
 librust-capstone-0.11.0+use-bindgen-dev (= ${binary:Version})
Description: Bindings to capstone disassembly engine - feature use_bindgen
 This metapackage enables feature "use_bindgen" for the Rust capstone crate, by
 pulling in any additional dependencies needed by that feature.
