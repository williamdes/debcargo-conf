commit fc5c42dd8d00acd651a53950d44b6284a00979b0
Author: Ola x Nilsson <olani@axis.com>
Date:   Fri Mar 17 15:20:29 2023 +0100

    gnu: Move struct timex_t to gnu/b* and support _TIME_BITS=64

diff --git a/src/unix/linux_like/linux/gnu/b32/mod.rs b/src/unix/linux_like/linux/gnu/b32/mod.rs
index 63e11d131f..471d7ad41c 100644
--- a/src/unix/linux_like/linux/gnu/b32/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b32/mod.rs
@@ -1,5 +1,6 @@
 //! 32-bit specific definitions for linux-like values
 
+use ntptimeval;
 use pthread_mutex_t;
 
 pub type c_long = i32;
@@ -16,6 +17,7 @@ pub type __fsword_t = i32;
 pub type fsblkcnt64_t = u64;
 pub type fsfilcnt64_t = u64;
 pub type __syscall_ulong_t = ::c_ulong;
+pub type __syscall_slong_t = ::c_long;
 
 cfg_if! {
     if #[cfg(target_arch = "riscv32")] {
@@ -161,6 +163,92 @@ s! {
         __glibc_reserved3: ::__syscall_ulong_t,
         __glibc_reserved4: ::__syscall_ulong_t,
     }
+
+    pub struct timex {
+        pub modes: ::c_uint,
+
+        #[cfg(not(gnu_time64_abi))]
+        pub offset: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub __unused_pad1: i32,
+        #[cfg(gnu_time64_abi)]
+        pub offset: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub freq: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub freq: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub maxerror: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub maxerror: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub esterror: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub esterror: ::c_longlong,
+        pub status: ::c_int,
+        #[cfg(not(gnu_time64_abi))]
+        pub constant: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub __unused_pad2: i32,
+        #[cfg(gnu_time64_abi)]
+        pub constant: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub precision: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub precision: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub tolerance: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub tolerance: ::c_longlong,
+        pub time: ::timeval,
+        #[cfg(not(gnu_time64_abi))]
+        pub tick: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub tick: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub ppsfreq: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub ppsfreq: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub jitter: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub jitter: ::c_longlong,
+        pub shift: ::c_int,
+        #[cfg(not(gnu_time64_abi))]
+        pub stabil: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub __unused_pad3: i32,
+        #[cfg(gnu_time64_abi)]
+        pub stabil: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub jitcnt: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub jitcnt: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub calcnt: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub calcnt: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub errcnt: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub errcnt: ::c_longlong,
+        #[cfg(not(gnu_time64_abi))]
+        pub stbcnt: ::__syscall_slong_t,
+        #[cfg(gnu_time64_abi)]
+        pub stbcnt: ::c_longlong,
+        pub tai: ::c_int,
+        pub __unused1: i32,
+        pub __unused2: i32,
+        pub __unused3: i32,
+        pub __unused4: i32,
+        pub __unused5: i32,
+        pub __unused6: i32,
+        pub __unused7: i32,
+        pub __unused8: i32,
+        pub __unused9: i32,
+        pub __unused10: i32,
+        pub __unused11: i32,
+    }
 }
 
 pub const POSIX_FADV_DONTNEED: ::c_int = 4;
diff --git a/src/unix/linux_like/linux/gnu/b64/mod.rs b/src/unix/linux_like/linux/gnu/b64/mod.rs
index b272ec0ca0..416e680e5d 100644
--- a/src/unix/linux_like/linux/gnu/b64/mod.rs
+++ b/src/unix/linux_like/linux/gnu/b64/mod.rs
@@ -1,5 +1,7 @@
 //! 64-bit specific definitions for linux-like values
 
+use ntptimeval;
+
 pub type ino_t = u64;
 pub type off_t = i64;
 pub type blkcnt_t = i64;
@@ -9,11 +11,16 @@ pub type msglen_t = u64;
 pub type fsblkcnt_t = u64;
 pub type fsfilcnt_t = u64;
 pub type rlim_t = u64;
-#[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-pub type __syscall_ulong_t = ::c_ulonglong;
-#[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-pub type __syscall_ulong_t = ::c_ulong;
 
+cfg_if! {
+    if #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))] {
+        pub type __syscall_ulong_t = ::c_ulonglong;
+        pub type __syscall_slong_t = ::c_longlong;
+    } else {
+        pub type __syscall_ulong_t = ::c_ulong;
+        pub type __syscall_slong_t = ::c_long;
+    }
+}
 cfg_if! {
     if #[cfg(all(target_arch = "aarch64", target_pointer_width = "32"))] {
         pub type clock_t = i32;
@@ -91,6 +98,46 @@ s! {
         __glibc_reserved3: ::__syscall_ulong_t,
         __glibc_reserved4: ::__syscall_ulong_t,
     }
+
+    pub struct timex {
+        pub modes: ::c_uint,
+        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
+        pub __unused_pad1: i32,
+        pub offset: ::__syscall_slong_t,
+        pub freq: ::__syscall_slong_t,
+        pub maxerror: ::__syscall_slong_t,
+        pub esterror: ::__syscall_slong_t,
+        pub status: ::c_int,
+        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
+        pub __unused_pad2: i32,
+        pub constant: ::__syscall_slong_t,
+        pub precision: ::__syscall_slong_t,
+        pub tolerance: ::__syscall_slong_t,
+        pub time: ::timeval,
+        pub tick: ::__syscall_slong_t,
+        pub ppsfreq: ::__syscall_slong_t,
+        pub jitter: ::__syscall_slong_t,
+        pub shift: ::c_int,
+        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
+        pub __unused_pad3: i32,
+        pub stabil: ::__syscall_slong_t,
+        pub jitcnt: ::__syscall_slong_t,
+        pub calcnt: ::__syscall_slong_t,
+        pub errcnt: ::__syscall_slong_t,
+        pub stbcnt: ::__syscall_slong_t,
+        pub tai: ::c_int,
+        pub __unused1: i32,
+        pub __unused2: i32,
+        pub __unused3: i32,
+        pub __unused4: i32,
+        pub __unused5: i32,
+        pub __unused6: i32,
+        pub __unused7: i32,
+        pub __unused8: i32,
+        pub __unused9: i32,
+        pub __unused10: i32,
+        pub __unused11: i32,
+    }
 }
 
 pub const __SIZEOF_PTHREAD_RWLOCKATTR_T: usize = 8;
diff --git a/src/unix/linux_like/linux/gnu/mod.rs b/src/unix/linux_like/linux/gnu/mod.rs
index a47c7f03bf..0dd3ad6d0f 100644
--- a/src/unix/linux_like/linux/gnu/mod.rs
+++ b/src/unix/linux_like/linux/gnu/mod.rs
@@ -198,84 +198,6 @@ s! {
         pub rt_irtt: ::c_ushort,
     }
 
-    pub struct timex {
-        pub modes: ::c_uint,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub offset: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub offset: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub freq: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub freq: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub maxerror: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub maxerror: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub esterror: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub esterror: ::c_long,
-        pub status: ::c_int,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub constant: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub constant: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub precision: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub precision: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub tolerance: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub tolerance: ::c_long,
-        pub time: ::timeval,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub tick: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub tick: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub ppsfreq: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub ppsfreq: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub jitter: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub jitter: ::c_long,
-        pub shift: ::c_int,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub stabil: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub stabil: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub jitcnt: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub jitcnt: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub calcnt: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub calcnt: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub errcnt: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub errcnt: ::c_long,
-        #[cfg(all(target_arch = "x86_64", target_pointer_width = "32"))]
-        pub stbcnt: i64,
-        #[cfg(not(all(target_arch = "x86_64", target_pointer_width = "32")))]
-        pub stbcnt: ::c_long,
-        pub tai: ::c_int,
-        pub __unused1: i32,
-        pub __unused2: i32,
-        pub __unused3: i32,
-        pub __unused4: i32,
-        pub __unused5: i32,
-        pub __unused6: i32,
-        pub __unused7: i32,
-        pub __unused8: i32,
-        pub __unused9: i32,
-        pub __unused10: i32,
-        pub __unused11: i32,
-    }
 
     pub struct ntptimeval {
         pub time: ::timeval,
